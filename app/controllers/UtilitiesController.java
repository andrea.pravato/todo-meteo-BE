package controllers;
import play.Logger;
import play.mvc.*;
import utilities.Geocoding;
import utilities.MyHeaders;

@With(MyHeaders.class)
public class UtilitiesController extends Controller{
    public Result geo(String address) {
        Logger.debug("Result= " + address);
        // Use key from app conf file
        // invoke utility class

        try {
            Geocoding.testSimpleGeocode();
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return ok("Geo under construction");
    };

    public Result weatherForecast() {
        // Use key from app conf file
        // invoke utility class
        // Use https://github.com/200Puls/darksky-forecast-api in utility class
        return ok("Forecast under construction");
    }
}




