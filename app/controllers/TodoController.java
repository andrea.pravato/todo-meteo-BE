package controllers;

import models.Todo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

import static play.libs.Scala.asScala;

import play.libs.Json;
import utilities.MyHeaders;

/**
 * An example of form processing.
 *
 * https://playframework.com/documentation/latest/JavaForms
 */
// @Singleton
// TODO remove MyHeaders and configure a reverse proxy instead
@With(MyHeaders.class)
public class TodoController extends Controller {

    private final Form<TodoData> form;
    private final List<Todo> todos;

    @Inject
    public TodoController(FormFactory formFactory) {
        this.form = formFactory.form(TodoData.class);
        this.todos = com.google.common.collect.Lists.newArrayList(
                new Todo("Test1", "Rovereto", 1507987363, 45.88204, 11.03647, "Sunny"),
                new Todo("Test2", "Trento", 1507987363, 45.88204, 11.03647, "Rainy"),
                new Todo("Test3", "Bolzano", 1507987363, 45.88204, 11.03647, "Cloudy")
        );
    }

//    public Result index() {
//        return ok(views.html.index.render());
//    }

    public Result listTodos() {
        // TODO perform query to retrieve items fromm db
        return ok(Json.toJson(todos));
//        .withHeader("Access-Control-Allow-Origin" , "*")
    }

    public Result createTodo() {
        final Form<TodoData> boundForm = form.bindFromRequest();

        if (boundForm.hasErrors()) {
            play.Logger.ALogger logger = play.Logger.of(getClass());
            logger.error("errors = {}", boundForm.errors());
            return badRequest(boundForm.errorsAsJson());
        } else {
            TodoData data = boundForm.get();
            // TODO perform query to store in the db
            todos.add(new Todo(data.getTitle(), data.getLocationAddress(), data.getTimestamp(), data.getLocationLat(), data.getLocationLong(), data.getWeatherForecast()));
            return ok("Ok, stored");
        }
    }
}