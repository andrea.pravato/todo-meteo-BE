package controllers;

import play.data.validation.Constraints;

/**
 * A form processing DTO that maps to the todo form.
 *
 * Using a class specifically for form binding reduces the chances
 * of a parameter tampering attack and makes code clearer, because
 * you can define constraints against the class.
 */
public class TodoData {

    @Constraints.Required
    private String title;

    // Following fields are optional
    private String locationAddress;

    // I am assuming that only quite "future" timestamps;
    // Using "System.currentTimeMillis() / 1000L" I was getting "element value must be a constant expression"
    @Constraints.Min(1507852800)
    private long timestamp;

    public double locationLat;
    public double locationLong;
    public String weatherForecast;

    public TodoData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getLocationLat() {
        return locationLat;
    }

    public double getLocationLong() {
        return locationLong;
    }

    public String getWeatherForecast() {
        return weatherForecast;
    }

    public void setLocationLat(double locationLat) {
        this.locationLat = locationLat;
    }

    public void setLocationLong(double locationLong) {
        this.locationLong = locationLong;
    }

    public void setWeatherForecast(String weatherForecast) {
        this.weatherForecast = weatherForecast;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }
}