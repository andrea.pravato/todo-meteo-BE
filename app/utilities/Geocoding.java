package utilities;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;

import javax.inject.Singleton;

@Singleton
public class Geocoding {
    private static final GeoApiContext context = new GeoApiContext()
            .setApiKey("");
    private static GeocodingResult[] results = new GeocodingResult[0];
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();


    public static void testSimpleGeocode() throws Exception {

        try{
            // This call can throw exception
            results =  GeocodingApi.geocode(context,
                "Rovereto").await();
        }catch(Throwable t){
            try {
                // Perform some application logging or auditing
                t.printStackTrace();
            } catch (Throwable tAppDebug) {
                tAppDebug.printStackTrace();
                throw t;
            }
        }
        if(results.length > 0){
            System.out.println(gson.toJson(results[0].addressComponents));
        }

    }


}