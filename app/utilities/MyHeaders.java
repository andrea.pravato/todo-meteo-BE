package utilities;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class MyHeaders extends Action.Simple {

    public CompletionStage<Result> call(final Http.Context ctx){
            ctx.response().setHeader("Access-Control-Allow-Origin", "*");
            return delegate.call(ctx);
    }
}