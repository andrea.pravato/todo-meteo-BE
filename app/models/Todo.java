package models;

/**
 * Presentation object used for displaying data in a template.
 *
 */
public class Todo {
    public String title;
    public String locationAddress;
    public long timestamp;
    public double locationLat;
    public double locationLong;
    public String weatherForecast;

    public Todo(String title, String locationAddress, long timestamp, double locationLat, double locationLong, String weatherForecast) {
        this.title = title;
        this.locationAddress = locationAddress;
        this.timestamp = timestamp;
        this.locationLat = locationLat;
        this.locationLong = locationLong;
        this.weatherForecast = weatherForecast;
    }
}