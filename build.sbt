name := "todo-meteo-be"
 
version := "1.0" 
      
lazy val `todo-meteo-be` = (project in file(".")).enablePlugins(PlayJava)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
scalaVersion := "2.11.11"

libraryDependencies ++=
  Seq( javaJdbc , cache , javaWs )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

// https://mvnrepository.com/artifact/com.google.maps/google-maps-services
libraryDependencies += "com.google.maps" % "google-maps-services" % "0.1.6"

// https://mvnrepository.com/artifact/com.orientechnologies/orientdb-graphdb
// libraryDependencies += "com.orientechnologies" % "orientdb-graphdb" % "2.2.29"